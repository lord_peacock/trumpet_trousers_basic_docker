#!/bin/bash

echo "Containers will now spawn with different IP addresses"

while true; do

    docker run --net mynet123 --ip 172.18.0.21 --name first -it -d ubuntu bin/bash
    echo "You now ping 172.18.0.21"
    sleep 30
    docker kill first
    echo "172.18.0.21 is now down"
    docker run --net mynet123 --ip 172.18.0.22 --name second -it -d ubuntu bin/bash
    echo "You now ping 172.18.0.22"
    sleep 30
    docker kill second
    echo "172.18.0.22 is now down"
    docker run --net mynet123 --ip 172.18.0.23 --name third -it -d ubuntu bin/bash 
    echo "You now ping 172.18.0.23"
    sleep 30
    docker kill third
    echo "172.18.0.22 is now down"

done






